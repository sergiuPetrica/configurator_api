<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CpuCooler extends Model {
    public const COOLING_SYSTEM_TYPES = [
        'air' => 0,
        'liquid' => 1,
        'phase_change' => 2
    ];

    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'cooling_system_active',
        'cooling_system_type',
        'heatpipe_count',
        'heatpipe_size',
        'height',
        'fan_size',
        'radiator_size',
        'pwm_fan',
        'fan_speed',
        'airflow',
        'fan_noise_level',
        'air_pressure',
        'fan_connector',
        'weight',
        'input_voltage',
        'tdp',
        'power_consumption'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function sockets() {
        return $this->belongsToMany(Socket::class);
    }
}
