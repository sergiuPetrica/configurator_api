<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MemoryType extends Model {
    protected $fillable = [
        'name'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function mainboards() {
        return $this->hasMany(Mainboard::class);
    }

    public function ram_kits() {
        return $this->hasMany(RamKit::class);
    }
}
