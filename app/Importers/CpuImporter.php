<?php

namespace App\Importers;

use App\Cpu;
use App\Manufacturer;
use App\MemoryType;
use App\Socket;

class CpuImporter {
    public function import(array $cpus): void {
        foreach ($cpus as $cpu) {
            $manufacturer = Manufacturer::firstOrCreate(
                [
                    'name' => $cpu['manufacturer']['name']
                ],
                [
                    'description' => $cpu['manufacturer']['description']
                ]
            );

            $cpu['manufacturer_id'] = $manufacturer->id;

            $socket = Socket::firstOrCreate(
                [
                    'name' => $cpu['socket']['name']
                ],
                []
            );

            $cpu['socket_id'] = $socket->id;

            $memory_type = MemoryType::firstOrCreate(
                [
                    'name' => $cpu['memory_type']['name']
                ],
                []
            );

            $cpu['memory_type_id'] = $memory_type->id;

            $db_cpu = Cpu::firstOrNew(
                [
                    'name' => $cpu['name'],
                    'manufacturer_id' => $manufacturer->id
                ],
                []
            );

            $db_cpu->fill($cpu);
            $db_cpu->save();
        }
    }
}
