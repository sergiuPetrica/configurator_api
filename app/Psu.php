<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Psu extends Model {

    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'oem_platform',
        'type',
        'power',
        'fans',
        'voltage',
        'pfc',
        'efficiency',
        'rating',
        'dimensions',
        'modularity',
        'protections',
        '12v_rail_count',
        '33v_amps',
        '5v_amps',
        '12v_amps',
        'negative_12v_amps',
        '5vsb_amps'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }
}
