<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GpuChipset extends Model {
    protected $fillable = [
        'name',
        'manufacturer_id',
        'memory_type',
        'series',
        'codename',
        'fab_process',
        'die_size',
        'tmu_count',
        'rops_count',
        'cu_count',
        'core_count',
        'transistor_count',
        'l1_cache',
        'l2_cache',
        'fp16_performance',
        'fp32_performance',
        'fp64_performance',
        'release_date'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function gpus() {
        return $this->hasMany(Gpu::class, 'chipset_id', 'id');
    }
}
