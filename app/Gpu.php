<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Gpu extends Model {
    public const COOLING_SYSTEM_TYPES = [
        'air' => 0,
        'liquid' => 1,
        'phase_change' => 2
    ];

    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'expansion_interface_id',
        'max_resolution',
        'chipset_id',
        'base_clock_mhz',
        'boost_clock_mhz',
        'tdp',
        'memory_shared',
        'memory_amount_mb',
        'memory_bus',
        'memory_frequency_mhz',
        'memory_frequency_effective_mhz',
        'memory_bandwidth',
        'pixel_fill_rate',
        'texture_fill_rate',
        'vga_port_count',
        'dvi_port_count',
        'hdmi_port_count',
        'dp_port_count',
        'mini_hdmi_port_count',
        'mini_dp_port_count',
        'shader_model',
        'dx_support',
        'opengl_support',
        'opencl_support',
        'vulkan_support',
        'freesync_support',
        'gsync_support',
        'cooling_system_active',
        'cooling_system_type',
        'cooling_system_slots',
        'length',
        'power_connectors',
        'release_date'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function expansion_interface() {
        return $this->belongsTo(ExpansionInterface::class);
    }

    public function chipset() {
        return $this->belongsTo(GpuChipset::class, 'chipset_id');
    }
}
