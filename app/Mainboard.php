<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainboard extends Model {
    protected $fillable = [
        'name',
        'series',
        'form_factor',
        'size',
        'manufacturer_id',
        'manufacturer_code',
        'chipset_id',
        'socket_id',
        'raid',
        'igpu',
        'audio_chipset',
        'audio_channels',
        'network_chipset',
        'network_speed',
        'memory_type_id',
        'memory_slot_size',
        'memory_max_amount_gb',
        'memory_slots',
        'memory_channels',
        'memory_max_speed_mhz',
        'memory_ecc',
        'crossfire',
        'sli',
        'm2_slots',
        'io_video',
        'io_audio',
        'io_usb',
        'io_lan',
        'io_misc',
        'internal_audio',
        'internal_usb',
        'internal_fans',
        'internal_storage',
        'internal_power',
        'internal_misc',
        'bluetooth',
        'wifi',
        'pcb_color',
        'led_color',
        'other_colors',
        'market_segment',
        'release_date'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function chipset() {
        return $this->belongsTo(MainboardChipset::class);
    }

    public function socket() {
        return $this->belongsTo(Socket::class);
    }

    public function memory_type() {
        return $this->belongsTo(MemoryType::class);
    }

    public function expansion_interfaces() {
        return $this->belongsToMany(ExpansionInterface::class)->withPivot('number_of_slots');
    }

    public function storage_interfaces() {
        return $this->belongsToMany(StorageInterface::class)->withPivot('number_of_slots');
    }
}
