<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RamKit extends Model {
    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'memory_type_id',
        'capacity',
        'frequency',
        'cas_latency',
        'voltage'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function memory_type() {
        return $this->belongsTo(MemoryType::class);
    }
}
