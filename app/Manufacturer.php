<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model {
    protected $fillable = [
        'name',
        'description'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function mainboards() {
        return $this->hasMany(Mainboard::class);
    }

    public function mainboard_chipsets() {
        return $this->hasMany(MainboardChipset::class);
    }

    public function cpus() {
        return $this->hasMany(Cpu::class);
    }

    public function gpus() {
        return $this->hasMany(Gpu::class);
    }

    public function gpu_chipsets() {
        return $this->hasMany(GpuChipset::class);
    }

    public function hdds() {
        return $this->hasMany(Hdd::class);
    }

    public function ssds() {
        return $this->hasMany(Ssd::class);
    }

    public function psus() {
        return $this->hasMany(Psu::class);
    }

    public function computer_cases() {
        return $this->hasMany(ComputerCase::class);
    }

    public function cpu_coolers() {
        return $this->hasMany(CpuCooler::class);
    }
}


