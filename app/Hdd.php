<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Hdd extends Model {
    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'storage_interface_id',
        'capacity',
        'buffer_size',
        'rpm',
        'form_factor'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function storage_interface() {
        return $this->belongsTo(StorageInterface::class);
    }
}
