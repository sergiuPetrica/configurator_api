<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ssd extends Model {

    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'storage_interface_id',
        'capacity',
        'controller_name',
        'read_speed',
        'write_speed',
        'form_factor'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function storage_interface() {
        return $this->belongsTo(StorageInterface::class);
    }
}
