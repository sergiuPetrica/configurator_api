<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ComputerCase extends Model {

    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'type',
        'psu_position',
        'included_psu',
        'colour',
        '35_inch_bay_count',
        '25_inch_bay_count',
        'expansion_slot_count',
        'radiator_support',
        'max_cpu_cooler_height',
        'max_gpu_length',
        'dimensions',
        'led_illumination',
        'transparent_side_panel',
        'ports',
        'included_fan_count',
        'max_fan_count',
        'included_fans',
        'optional_fans'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }
}
