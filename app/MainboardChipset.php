<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MainboardChipset extends Model {
    protected $fillable = [
        'name',
        'manufacturer_id'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function mainboards() {
        return $this->hasMany(Mainboard::class, 'chipset_id');
    }
}
