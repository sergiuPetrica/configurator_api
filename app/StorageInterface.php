<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StorageInterface extends Model {

    protected $fillable = [
        'name'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function mainboards() {
        return $this->belongsToMany(Mainboard::class)->withPivot('number_of_slots');
    }

    public function hdds() {
        return $this->hasMany(Hdd::class);
    }

    public function ssds() {
        return $this->hasMany(Ssd::class);
    }
}
