<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Socket extends Model {
    protected $fillable = [
        'name'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function cpus() {
        return $this->hasMany(Cpu::class);
    }

    public function mainboards() {
        return $this->hasMany(Mainboard::class);
    }

    public function cpu_coolers() {
        return $this->belongsToMany(CpuCooler::class);
    }
}
