<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpansionInterface extends Model {
    protected $fillable = [
        'name'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function mainboards() {
        return $this->belongsToMany(Mainboard::class)->withPivot('number_of_slots');
    }

    public function gpus() {
        return $this->hasMany(Gpu::class);
    }
}
