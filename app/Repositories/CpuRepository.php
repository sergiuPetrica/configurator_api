<?php

namespace App\Repositories;

use App\Interfaces\JsonImportInterface;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CpuRepository implements JsonImportInterface {
    private ResourceCollection $resource;

    public function __construct(ResourceCollection $resource) {
        $this->resource = $resource;
    }

    public function getFromJson(string $filepath): array {
        $content = file_get_contents($filepath);
        $content = json_decode($content, true);
        $content = $this->resource::make(collect($content))->resolve();

        return $content;
    }
}
