<?php

namespace App\Repositories\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BaseRepositoryResource extends ResourceCollection {
    public static function makeEmpty() {
        return new static(collect([]));
    }
}
