<?php

namespace App\Repositories\Resources;

use Carbon\Carbon;

class CpuCollectionTechpowerup extends BaseRepositoryResource {
    public function toArray($request): array {
        $result = [];

        foreach ($this->resource as $resource) {
            $manufacturer = [
                'name' => explode(' ', $resource['name'])[0],
                'description' => null
            ];

            $socket_name = explode(' ', $resource['socket']);
            $socket = [
                'name' => end($socket_name)
            ];

            $memory_type = [
                'name' => $resource['supported_memory']
            ];

            $base_frequency_mhz = $this->processBaseFrequency($resource['base_frequency']);
            $turbo_frequency_mhz = $this->processTurboFrequency($resource['turbo_frequency']);
            $tdp = (int)filter_var($resource['tdp'], FILTER_SANITIZE_NUMBER_INT);
            $unlocked = strtolower($resource['unlocked']) === 'yes';
            $release_date = Carbon::parse($resource['release_date'])->format('Y-m-d');

            $data = [
                'name' => $resource['cpu'],
                'manufacturer' => $manufacturer,
                'manufacturer_code' => null,
                'socket' => $socket,
                'codename' => $resource['codename'],
                'generation' => $resource['generation'],
                'fab_process' => strtolower($resource['process_size']) === 'unknown' ? null : $resource['process_size'],
                'die_size' => strtolower($resource['die_size']) === 'unknown' ? null : $resource['die_size'],
                'core_count' => $resource['core_count'],
                'thread_count' => $resource['thread_count'],
                'smp_count' => $resource['smp_number_cpus'],
                'base_frequency_mhz' => $base_frequency_mhz,
                'turbo_frequency_mhz' => $turbo_frequency_mhz,
                'l1_cache' => $resource['l1_cache'],
                'l2_cache' => $resource['l2_cache'],
                'l3_cache' => $resource['l3_cache'],
                'tdp' => $tdp,
                'includes_cooler' => null,
                'unlocked' => $unlocked,
                'memory_type' => $memory_type,
                'memory_max_amount_gb' => null,
                'memory_frequency_mhz' => null,
                'memory_channels' => null,
                'tcase_max' => strtolower($resource['t_case_max']) === 'unknown' ? null : $resource['t_case_max'],
                'igpu' => strtolower($resource['igpu']) === 'n/a' ? null : $resource['igpu'],
                'market_segment' => $resource['market'],
                'release_date' => $release_date
            ];

            $result[] = $data;
        }

        return $result;
    }

    private function processBaseFrequency(string $base_frequency): ?int {
        $base_frequency_contains_ghz = strpos(strtolower($base_frequency), 'ghz');
        $base_frequency = explode(' ', $base_frequency)[0];

        if ($base_frequency_contains_ghz) {
            $base_frequency_mhz = (float)$base_frequency * 1000;
        } else {
            $base_frequency_mhz = $base_frequency;
        }

        return (int)$base_frequency_mhz;
    }

    private function processTurboFrequency(string $turbo_frequency): ?int {
        if (strtolower($turbo_frequency) === 'n/a') {
            return null;
        }

        $turbo_frequency_contains_ghz = strpos(strtolower($turbo_frequency), 'ghz');
        if ($turbo_frequency_contains_ghz) {
            $turbo_frequency_mhz = (float)filter_var($turbo_frequency, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) * 1000;
        } else {
            $turbo_frequency_mhz = (int)filter_var($turbo_frequency, FILTER_SANITIZE_NUMBER_INT);
        }

        return $turbo_frequency_mhz;
    }
}
