<?php

namespace App\Repositories\Resources;

class CpuCollectionNewegg extends BaseRepositoryResource {
    public function toArray($request): array {
        $result = [];

        foreach ($this->resource as $resource) {
            $manufacturer = [
                'name' => $resource['manufacturer'],
                'description' => null
            ];

            $socket = [
                'name' => $resource['socket']
            ];

            $memory = explode(' ', $resource['supported_memory']);

            $memory_type = [
                'name' => $memory[0]
            ];

            $memory_frequency_mhz = $memory[1];
            $core_count = (int)filter_var($resource['core_count'], FILTER_SANITIZE_NUMBER_INT);
            $base_frequency_mhz = $this->processFrequency($resource['base_frequency']);
            $turbo_frequency_mhz = $this->processFrequency($resource['base_frequency']);
            $tdp = (int)filter_var($resource['tdp'], FILTER_SANITIZE_NUMBER_INT);
            $includes_cooler = strpos($resource['includes_cooler'], 'Heatsink and fan included') !== false ? true : false;

            $data = [
                'name' => $resource['name'],
                'manufacturer' => $manufacturer,
                'manufacturer_code' => $resource['manufacturer_code'],
                'socket' => $socket,
                'codename' => $resource['codename'],
                'generation' => null,
                'fab_process' => $resource['process_node'],
                'die_size' => null,
                'core_count' => $core_count,
                'thread_count' => $resource['thread_count'],
                'smp_count' => null,
                'base_frequency_mhz' => $base_frequency_mhz,
                'turbo_frequency_mhz' => $turbo_frequency_mhz,
                'l1_cache' => $resource['l1_cache'],
                'l2_cache' => $resource['l2_cache'],
                'l3_cache' => $resource['l3_cache'],
                'tdp' => $tdp,
                'includes_cooler' => $includes_cooler,
                'unlocked' => null,
                'memory_type' => $memory_type,
                'memory_max_amount_gb' => null,
                'memory_frequency_mhz' => $memory_frequency_mhz,
                'memory_channels' => $resource['memory_channels'],
                'tcase_max' => null,
                'igpu' => $resource['igpu'],
                'market_segment' => $resource['market'],
                'release_date' => null
            ];

            $result[] = $data;
        }

        return $result;
    }

    private function processFrequency(string $base_frequency): ?int {
        $base_frequency_contains_ghz = strpos(strtolower($base_frequency), 'ghz');
        $base_frequency = explode(' ', $base_frequency)[0];

        if ($base_frequency_contains_ghz) {
            $base_frequency_mhz = (float)$base_frequency * 1000;
        } else {
            $base_frequency_mhz = $base_frequency;
        }

        return (int)$base_frequency_mhz;
    }
}
