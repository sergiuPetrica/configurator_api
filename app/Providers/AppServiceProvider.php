<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // TODO: remove if you're not using this in 2020.
//        if ($this->app->environment() == 'local') {
//            $this->app->register('Wn\Generators\CommandsServiceProvider');
//        }
    }
}
