<?php

namespace App\Interfaces;

use Illuminate\Http\Resources\Json\ResourceCollection;

interface JsonImportInterface {
    public function __construct(ResourceCollection $resource);
    public function getFromJson(string $filepath): array;
}
