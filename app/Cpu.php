<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cpu extends Model {
    protected $fillable = [
        'name',
        'manufacturer_id',
        'manufacturer_code',
        'socket_id',
        'codename',
        'generation',
        'fab_process',
        'die_size',
        'core_count',
        'thread_count',
        'smp_count',
        'base_frequency_mhz',
        'turbo_frequency_mhz',
        'l1_cache',
        'l2_cache',
        'l3_cache',
        'tdp',
        'includes_cooler',
        'unlocked',
        'memory_type_id',
        'memory_max_amount_gb',
        'memory_frequency_mhz',
        'memory_channels',
        'tcase_max',
        'igpu',
        'market_segment',
        'release_date'
    ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class);
    }

    public function socket() {
        return $this->belongsTo(Socket::class);
    }

    public function memory_type() {
        return $this->belongsTo(MemoryType::class);
    }
}
