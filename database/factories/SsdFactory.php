<?php

use App\Manufacturer;
use App\StorageInterface;

$factory->define(App\Ssd::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    if (!isset($params['storage_interface_id'])) {
        $storage_interface = factory(StorageInterface::class)->create()->toArray();
    } else {
        $storage_interface['id'] = $params['storage_interface_id'];
    }

    return [
        'name' => $params['name'] ?? ($faker->boolean ? 'Kingston HyperX Fury' : 'Samsung 860 Evo'),
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'storage_interface_id' => $storage_interface['id'],
        'capacity' => $faker->boolean() ? '512 GB' : '1 TB',
        'controller_name' => $faker->boolean() ? 'Phison' : 'Marvell',
        'read_speed' => '500 MB/s',
        'write_speed' => '500MB/s',
        'form_factor' => '2.5 in'
    ];
});
