<?php

use App\MainboardChipset;
use App\Manufacturer;
use App\MemoryType;
use App\Socket;

$factory->define(App\Mainboard::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    if (!isset($params['chipset_id'])) {
        $chipset = factory(MainboardChipset::class)->create()->toArray();
    } else {
        $chipset['id'] = $params['chipset_id'];
    }

    if (!isset($params['memory_type'])) {
        $memory_type = factory(MemoryType::class)->create()->toArray();
    } else {
        $memory_type['id'] = $params['memory_type_id'];
    }

    if (!isset($params['socket_id'])) {
        $socket = factory(Socket::class)->create()->toArray();
    } else {
        $socket['id'] = $params['socket_id'];
    }

    $io_video = [
        'DVI' => 1,
        'HDMI' => 2,
        'DP' => 2
    ];

    $io_audio = [
        'Analog Audio Jack' => 3
    ];

    $io_usb = [
        '3.0' => 4,
        '3.1' => 4
    ];

    $io_lan = [
        'RJ-45' => 1
    ];

    $io_misc = [
        'PS/2' => 1
    ];

    $internal_audio = [
        'SPDIF Out Header' => 1
    ];

    $internal_usb = [
        'USB 2.0 Header' => 1,
        'USB 3.0 Header' => 2
    ];

    $internal_fans = [
        '4-pin Fan Header' => 3
    ];

    $internal_storage = [
        'SATA3' => 4
    ];

    $internal_power = [
        '24-pin ATX Power' => 1,
        '8-pin 12V Power' => 1
    ];

    $internal_misc = [
        'RGB Header' => 2,
        'TPM Header' => 1,
        'Thunderbolt 5-pin Header' => 1,
        'VROC Header' => 1
    ];

    return [
        'name' => $faker->boolean() ? 'AsRock X570 Taichi' : 'ASUS TUF Gaming X570',
        'series' => 'Some Series',
        'form_factor' => $faker->boolean() ? 'ATX' : 'mATX',
        'size' => '60 x 40',
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $faker->postcode,
        'chipset_id' => $chipset['id'],
        'socket_id' => $socket['id'],
        'raid' => '0, 1, 5, 10',
        'igpu' => 'Requires CPU with ' . ($faker->boolean() ? 'Intel ' : 'AMD ') . 'Integrated Graphics',
        'audio_chipset' => 'Realtek ALC892',
        'audio_channels' => '7.1',
        'network_chipset' => 'Intel NIC',
        'network_speed' => '1Gbps',
        'memory_type_id' => $memory_type['id'],
        'memory_slot_size' => '48-pin',
        'memory_max_amount_gb' => 32,
        'memory_slots' => 4,
        'memory_channels' => 2,
        'memory_max_speed_mhz' => 2400,
        'memory_ecc' => false,
        'crossfire' => false,
        'sli' => false,
        'm2_slots' => 2,
        'io_video' => json_encode($io_video),
        'io_audio' => json_encode($io_audio),
        'io_usb' => json_encode($io_usb),
        'io_lan' => json_encode($io_lan),
        'io_misc' => json_encode($io_misc),
        'internal_audio' => json_encode($internal_audio),
        'internal_usb' => json_encode($internal_usb),
        'internal_fans' => json_encode($internal_fans),
        'internal_storage' => json_encode($internal_storage),
        'internal_power' => json_encode($internal_power),
        'internal_misc' => json_encode($internal_misc),
        'bluetooth' => '',
        'wifi' => 'Wi-Fi 802.11 a/b/g/n/ac/ax, supporting 2.4/5 GHz Dual-Band',
        'pcb_color' => 'Black',
        'led_color' => 'RGB',
        'other_colors' => 'Bluetooth 5.0',
        'market_segment' => 'Mainstream',
        'release_date' => date('Y-m-d')
    ];
});
