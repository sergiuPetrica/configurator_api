<?php

use App\ExpansionInterface;
use App\Gpu;
use App\GpuChipset;
use App\Manufacturer;

$factory->define(App\Gpu::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    if (!isset($params['expansion_interface_id'])) {
        $expansion_interface = factory(ExpansionInterface::class)->create()->toArray();
    } else {
        $expansion_interface['id'] = $params['expansion_interface_id'];
    }

    if (!isset($params['chipset_id'])) {
        $chipset = factory(GpuChipset::class)->create()->toArray();
    } else {
        $chipset['id'] = $params['chipset_id'];
    }

    return [
        'name' => ($faker->boolean() ? 'GTX' : 'Radeon') . $faker->numberBetween(1000-9999),
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $faker->postcode,
        'expansion_interface_id' => $expansion_interface['id'],
        'max_resolution' => $faker->boolean() ? '5120x2880' : '7680x4320',
        'chipset_id' => $chipset['id'],
        'base_clock_mhz' => 1465,
        'boost_clock_mhz' => 1765,
        'tdp' => 150,
        'memory_shared' => false,
        'memory_amount_mb' => 8196,
        'memory_bus' => '256-bit',
        'memory_frequency_mhz' => 2000,
        'memory_frequency_effective_mhz' => 8000,
        'memory_bandwidth' => '448 GB/sec',
        'pixel_fill_rate' => '93760 MPixels/sec',
        'texture_fill_rate' => '210960 MTexels/sec',
        'vga_port_count' => 0,
        'dvi_port_count' => 1,
        'hdmi_port_count' => 2,
        'dp_port_count' => 2,
        'mini_hdmi_port_count' => 0,
        'mini_dp_port_count' => 0,
        'shader_model' => null,
        'dx_support' => 12.1,
        'opengl_support' => 4.5,
        'opencl_support' => null,
        'vulkan_support' => 1.0,
        'freesync_support' => true,
        'gsync_support' => false,
        'cooling_system_active' => true,
        'cooling_system_type' => Gpu::COOLING_SYSTEM_TYPES['air'],
        'cooling_system_slots' => 2,
        'length' => null,
        'power_connectors' => null,
        'release_date' => null
    ];
});
