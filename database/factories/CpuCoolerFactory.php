<?php

use App\CpuCooler;
use App\Manufacturer;

$factory->define(App\CpuCooler::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    return [
        'name' => $params['name'] ?? ($faker->boolean() ? 'Titan Fenrir' : 'Thermaltake Frio'),
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'cooling_system_active' => $params['manufacturer_code'] ?? ($faker->boolean() ? true : false),
        'cooling_system_type' => $params['cooling_system_type'] ?? ($faker->boolean()
            ? CpuCooler::COOLING_SYSTEM_TYPES['air']
            : CpuCooler::COOLING_SYSTEM_TYPES['liquid']),
        'heatpipe_count' => $params['heatpipe_count'] ?? ($faker->boolean() ? 4 : 6),
        'heatpipe_size' => $params['heatpipe_size'] ?? ($faker->boolean ? '8 mm' : '10 mm'),
        'height' => $params['height'] ?? ($faker->boolean() ? '160 mm' : '180 mm'),
        'fan_size' => $params['fan_size'] ?? ($faker->boolean() ? '120 mm' : '140 mm'),
        'radiator_size' => $params['radiator_size'] ?? '160 x 150 x 40 mm',
        'pwm_fan' => $params['pwm_fan'] ?? $faker->boolean() ? true : false,
        'fan_speed' => $params['fan_speed'] ?? '500-1200 RPM',
        'airflow' => $params['airflow'] ?? ($faker->boolean() ? '71 CFM' : '83 CFM'),
        'fan_noise_level' => $params['fan_noise_level'] ?? ($faker->boolean() ? '19.3 dBA' : '20.1 dBA'),
        'air_pressure' => $params['air_pressure'] ?? '1.13 mmH2O',
        'fan_connector' => $params['fan_connector'] ?? '4-pin',
        'weight' => $params['weight'] ?? ($faker->boolean() ? '620 g' : '710g'),
        'input_voltage' => $params['input_voltage'] ?? '12 V',
        'tdp' => $params['tdp'] ?? ($faker->boolean() ? '165 W' : '220 W'),
        'power_consumption' => $params['power_consumption'] ?? ($faker->boolean() ? '5.5 W' : '6.1 W')
    ];
});
