<?php

use App\Manufacturer;

$factory->define(App\MainboardChipset::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    return [
        'name' => $faker->boolean() ? 'AMD X570 ' : 'Intel Z390',
        'manufacturer_id' => $manufacturer['id'],
    ];
});
