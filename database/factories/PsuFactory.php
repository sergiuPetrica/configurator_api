<?php

use App\Manufacturer;

$factory->define(App\Psu::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    return [
        'name' => $params['name'] ?? $faker->macProcessor,
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'oem_platform' => 'Seasonic',
        'type' => 'ATX 2.3',
        'power' => $faker->boolean() ? '650 W' : '750W',
        'fans' => $faker->boolean() ? '1x120mm' : '1x140mm',
        'voltage' => '100-240Vac',
        'pfc' => 'Active',
        'efficiency' => $faker->boolean() ? '86%' : '89%',
        'rating' => $faker->boolean() ? '80+ Silver' : '80+ Gold',
        'dimensions' => '160 x 150 x 86 mm',
        'modularity' => 'Fully Modular',
        'protections' => 'OPP, OVP, SCP, UVP',
        '12v_rail_count' => 1,
        '33v_amps' => '24 A',
        '5v_amps' => '24 A',
        '12v_amps' => '48 A',
        'negative_12v_amps' => '0.8 A',
        '5vsb_amps' => '2.5 A'
    ];
});
