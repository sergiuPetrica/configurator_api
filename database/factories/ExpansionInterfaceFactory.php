<?php

$factory->define(App\ExpansionInterface::class, function (Faker\Generator $faker, array $params) {
    return [
        'name' => 'PCI-E x16' . ($faker->boolean() ? '3.0' : '4.0')
    ];
});
