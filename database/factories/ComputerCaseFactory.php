<?php

use App\Manufacturer;

$factory->define(App\ComputerCase::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    $ports = [
        'usb_3.0' => 2,
        'usb_2.0' => 2,
        '35_mm_jack' => 2
    ];

    return [
        'name' => $params['name'] ?? ($faker->boolean() ? 'Fractal Design Define C' : 'Fractal Design Meshify C'),
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'type' => $params['type'] ?? ($faker->boolean() ? 'Full Tower' : 'Mid Tower'),
        'psu_position' => $params['psu_position'] ?? ($faker->boolean ? 'Top Left' : 'Bottom Left'),
        'included_psu' => $params['included_psu'] ?? 'No',
        'colour' => $params['colour'] ?? ($faker->boolean() ? 'Black' : 'White'),
        '35_inch_bay_count' => $params['35_inch_bay_count'] ?? ($faker->boolean ? 0 : 2),
        '25_inch_bay_count' => $params[''] ?? ($faker->boolean ? 0 : 1),
        'expansion_slot_count' => $params['expansion_slot_count'] ?? ($faker->boolean() ? 5 : 7),
        'radiator_support' => $params['radiator_support'] ?? 'Front - 1x 120/240mm',
        'max_cpu_cooler_height' => $params['max_cpu_cooler_height'] ?? ($faker->boolean() ? '135 mm' : '150 mm'),
        'max_gpu_length' => $params['max_gpu_length'] ?? ($faker->boolean() ? '355 mm' : '380 mm'),
        'dimensions' => $params['dimensions'] ?? '425 x 435 x 190 mm',
        'led_illumination' => $params['led_illumination'] ?? ($faker->boolean() ? 'No' : 'RGB'),
        'transparent_side_panel' => $params['transparent_side_panel'] ?? ($faker->boolean() ? 'No' : 'Yes, plexiglass'),
        'ports' => $params['ports'] ?? json_encode($ports),
        'included_fan_count' => $params['included_fan_count'] ?? 2,
        'max_fan_count' => $params['max_fan_count'] ?? 4,
        'included_fans' => $params['included_fans'] ?? 'Front 2x 120 mm',
        'optional_fans' => $params['optional_fans'] ?? 'Back 1x 120 mm, Top 1x 160 mm'
    ];
});
