<?php

use App\Manufacturer;
use App\MemoryType;
use App\Socket;

$factory->define(App\Cpu::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    if (!isset($params['socket_id'])) {
        $socket = factory(Socket::class)->create()->toArray();
    } else {
        $socket['id'] = $params['socket_id'];
    }

    if (!isset($params['core_count'])) {
        $core_count = $faker->numberBetween(2, 16);
        $core_count = $core_count % 2 === 0
            ? $core_count
            : ($faker->numberBetween(0, 1) ? $core_count - 1 : $core_count + 1);
    } else {
        $core_count = $params['core_count'];
    }

    if (!isset($params['thread_count'])) {
        $thread_count = $faker->numberBetween(0, 1) ? $core_count : $core_count * 2;
    } else {
        $thread_count = $params['thread_count'];
    }

    if (!isset($params['base_frequency'])) {
        $base_frequency = $faker->randomNumber(4);
    } else {
        $base_frequency = $params['base_frequency'];
    }

    if (!isset($params['boost_frequency'])) {
        $boost_frequency = $base_frequency + $faker->randomNumber(3);
    } else {
        $boost_frequency = $params['boost_frequency'];
    }

    if (!isset($params['memory_type'])) {
        $memory_type = factory(MemoryType::class)->create()->toArray();
    } else {
        $memory_type['id'] = $params['memory_type_id'];
    }

    return [
        'name' => $params['name'] ?? $faker->macProcessor,
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'socket_id' => $socket['id'],
        'codename' => $params['codename'] ?? $faker->lastName,
        'generation' => 'Some Generation',
        'fab_process' => $params['fab_process'] ?? (string)$faker->numberBetween(4, 16) . ' nm',
        'die_size' => '500mm',
        'core_count' => $core_count,
        'thread_count' => $thread_count,
        'smp_count' => $thread_count,
        'base_frequency_mhz' => $params['base_frequency'] ?? (int)$base_frequency,
        'turbo_frequency_mhz' => $params['boost_frequency'] ?? (int)$boost_frequency,
        'l1_cache' => $params['l1_cache'] ?? (string)(2 * $faker->numberBetween(4, 8)) . ' KB',
        'l2_cache' => $params['l2_cache'] ?? (string)(2 * $faker->numberBetween(2, 4)) . ' MB',
        'l3_cache' => $params['l3_cache'] ?? (string)(2 * $faker->numberBetween(4, 8)) . ' MB',
        'tdp' => $params['tdp'] ?? $faker->numberBetween(25, 120),
        'includes_cooler' => $params['includes_cooler'] ?? $faker->boolean(),
        'unlocked' => $params['unlocked'] ?? $faker->boolean(),
        'memory_type_id' => $memory_type['id'],
        'memory_max_amount_gb' => $params['memory_max_amount_gb'] ?? pow(2, $faker->numberBetween(5, 7)),
        'memory_frequency_mhz' => $params['memory_frequency_mhz'] ?? 2400 + 400 * $faker->numberBetween(0, 4),
        'memory_channels' => $params['memory_channels'] ?? $faker->boolean(10) ? 2 : 4
    ];
});
