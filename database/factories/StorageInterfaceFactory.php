<?php

$factory->define(App\StorageInterface::class, function (Faker\Generator $faker, array $params) {
    return [
        'name' => $faker->boolean() ? 'SATA3' : 'M2'
    ];
});
