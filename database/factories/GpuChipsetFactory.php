<?php

use App\Manufacturer;

$factory->define(App\GpuChipset::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    return [
        'name' => $faker->boolean() ? 'Pascal' : 'Vega',
        'manufacturer_id' => $manufacturer['id'],
        'memory_type' => 'GDDR5',
        'series' => $faker->boolean() ? 'GeForce RTX 2000' : 'Radeon RX 500',
        'codename' => $faker->boolean() ? 'Navi' : 'Pascal',
        'fab_process' => $faker->boolean() ? '14nm' : '8nm',
        'die_size' => '750mm',
        'tmu_count' => 144,
        'rops_count' => 64,
        'cu_count' => 0,
        'core_count' => 2304,
        'transistor_count' => '1030 Million',
        'l1_cache' => '32KB',
        'l2_cache' => '16MB'
    ];
});
