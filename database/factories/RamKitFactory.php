<?php

use App\Manufacturer;
use App\MemoryType;

$factory->define(App\RamKit::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    if (!isset($params['memory_type'])) {
        $memory_type = factory(MemoryType::class)->create()->toArray();
    } else {
        $memory_type['id'] = $params['memory_type_id'];
    }

    return [
        'name' => $params['name'] ?? ($faker->boolean() ? 'Corsair Vengeance LPX Black' : 'G.Skill Trident Z'),
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'memory_type_id' => $memory_type['id'],
        'capacity' => $params['capacity'] ?? ($faker->boolean() ? '16 GB' : '32 GB'),
        'frequency' => $params['frequency'] ?? ($faker->boolean() ? '2800 MHz' : '3200 MHz'),
        'cas_latency' => $params['cas_latency'] ?? ($faker->boolean() ? 'CL 15' : 'CL 14'),
        'voltage' => $params['voltage'] ?? ($faker->boolean() ? '1.5 V' : '1.35 V')
    ];
});
