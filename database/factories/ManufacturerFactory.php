<?php

$factory->define(App\Manufacturer::class, function (Faker\Generator $faker, array $params) {
    return [
        'name' => $params['name'] ?? $faker->company,
        'description' => $params['description'] ?? $faker->realText()
    ];
});
