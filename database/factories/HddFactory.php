<?php

use App\Manufacturer;
use App\StorageInterface;

$factory->define(App\Hdd::class, function (Faker\Generator $faker, array $params) {
    if (!isset($params['manufacturer_id'])) {
        $manufacturer = factory(Manufacturer::class)->create()->toArray();
    } else {
        $manufacturer['id'] = $params['manufacturer_id'];
    }

    if (!isset($params['storage_interface_id'])) {
        $storage_interface = factory(StorageInterface::class)->create()->toArray();
    } else {
        $storage_interface['id'] = $params['storage_interface_id'];
    }

    return [
        'name' => $params['name'] ?? $faker->macProcessor,
        'manufacturer_id' => $manufacturer['id'],
        'manufacturer_code' => $params['manufacturer_code'] ?? $faker->postcode,
        'storage_interface_id' => $storage_interface['id'],
        'capacity' => $faker->boolean() ? '1 TB' : '2 TB',
        'buffer_size' => $faker->boolean() ? '32 MB' : '64 MB',
        'rpm' => $faker->boolean() ? '7200 RPM' : '10000 RPM',
        'form_factor' => $faker->boolean() ? '2.5 in' : '3.5 in',
    ];
});
