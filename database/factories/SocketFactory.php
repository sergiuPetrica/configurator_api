<?php

$factory->define(App\Socket::class, function (Faker\Generator $faker, array $params) {
    return [
        'name' => $params['name'] ?? $faker->randomNumber(),
    ];
});
