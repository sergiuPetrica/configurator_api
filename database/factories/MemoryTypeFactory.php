<?php

$factory->define(App\MemoryType::class, function (Faker\Generator $faker, array $params) {
    return [
        'name' => $faker->boolean() ? 'DDR3' : 'DDR4'
    ];
});
