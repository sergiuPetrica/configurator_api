<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHddTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hdds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code');
            $table->unsignedBigInteger('storage_interface_id');
            $table->string('capacity');
            $table->string('buffer_size');
            $table->string('rpm');
            $table->string('form_factor');
            $table->timestamps();
        });

        Schema::table('hdds', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
            $table->foreign('storage_interface_id')->references('id')->on('storage_interfaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hdds');
    }
}
