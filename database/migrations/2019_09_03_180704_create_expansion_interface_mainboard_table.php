<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpansionInterfaceMainboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expansion_interface_mainboard', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mainboard_id');
            $table->unsignedBigInteger('expansion_interface_id');
            $table->unsignedInteger('number_of_slots');
            $table->timestamps();
        });

        Schema::table('expansion_interface_mainboard', function (Blueprint $table) {
            $table->foreign('mainboard_id', 'mb_ei_foreign')->references('id')->on('mainboards');
            $table->foreign('expansion_interface_id', 'ei_mb_foreign')->references('id')->on('expansion_interfaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expansion_interface_mainboard');
    }
}
