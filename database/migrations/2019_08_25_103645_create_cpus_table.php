<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code')->nullable();
            $table->unsignedBigInteger('socket_id');
            $table->string('codename')->nullable();
            $table->string('generation')->nullable();
            $table->string('fab_process')->nullable();
            $table->string('die_size')->nullable();
            $table->unsignedSmallInteger('core_count');
            $table->unsignedSmallInteger('thread_count');
            $table->unsignedSmallInteger('smp_count')->nullable();
            $table->unsignedSmallInteger('base_frequency_mhz');
            $table->unsignedSmallInteger('turbo_frequency_mhz')->nullable();
            $table->string('l1_cache')->nullable();
            $table->string('l2_cache')->nullable();
            $table->string('l3_cache')->nullable();
            $table->unsignedSmallInteger('tdp');
            $table->boolean('includes_cooler')->nullable();
            $table->boolean('unlocked')->nullable();
            $table->unsignedBigInteger('memory_type_id');
            $table->unsignedSmallInteger('memory_max_amount_gb')->nullable();
            $table->unsignedSmallInteger('memory_frequency_mhz')->nullable();
            $table->unsignedTinyInteger('memory_channels')->nullable();
            $table->string('tcase_max')->nullable();
            $table->string('igpu')->nullable();
            $table->string('market_segment')->nullable();
            $table->date('release_date')->nullable();
            $table->timestamps();
        });

        Schema::table('cpus', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
            $table->foreign('socket_id')->references('id')->on('sockets');
            $table->foreign('memory_type_id')->references('id')->on('memory_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpus');
    }
}
