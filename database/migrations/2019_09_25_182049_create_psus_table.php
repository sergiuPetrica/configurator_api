<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('psus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code');
            $table->string('oem_platform');
            $table->string('type');
            $table->string('power');
            $table->string('fans');
            $table->string('voltage');
            $table->string('pfc');
            $table->string('efficiency');
            $table->string('rating');
            $table->string('dimensions');
            $table->string('modularity');
            $table->string('protections');
            $table->unsignedInteger('12v_rail_count');
            $table->string('33v_amps');
            $table->string('5v_amps');
            $table->string('12v_amps');
            $table->string('negative_12v_amps');
            $table->string('5vsb_amps');
            $table->timestamps();
        });

        Schema::table('psus', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psus');
    }
}
