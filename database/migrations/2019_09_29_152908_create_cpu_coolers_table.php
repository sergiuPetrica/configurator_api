<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpuCoolersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpu_coolers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code');
            $table->boolean('cooling_system_active');
            $table->unsignedTinyInteger('cooling_system_type');
            $table->unsignedTinyInteger('heatpipe_count');
            $table->string('heatpipe_size');
            $table->string('height');
            $table->string('fan_size');
            $table->string('radiator_size');
            $table->boolean('pwm_fan');
            $table->string('fan_speed');
            $table->string('airflow');
            $table->string('fan_noise_level');
            $table->string('air_pressure');
            $table->string('fan_connector');
            $table->string('weight');
            $table->string('input_voltage');
            $table->string('tdp');
            $table->string('power_consumption');
            $table->timestamps();
        });

        Schema::table('cpu_coolers', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpu_coolers');
    }
}
