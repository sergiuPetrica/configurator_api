<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpuChipsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gpu_chipsets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('memory_type');
            $table->string('series')->nullable();
            $table->string('codename')->nullable();
            $table->string('fab_process')->nullable();
            $table->string('die_size')->nullable();
            $table->unsignedSmallInteger('tmu_count')->nullable();
            $table->unsignedSmallInteger('rops_count')->nullable();
            $table->unsignedSmallInteger('cu_count')->nullable();
            $table->unsignedSmallInteger('core_count')->nullable();
            $table->string('transistor_count')->nullable();
            $table->string('l1_cache')->nullable();
            $table->string('l2_cache')->nullable();
            $table->string('fp16_performance')->nullable();
            $table->string('fp32_performance')->nullable();
            $table->string('fp64_performance')->nullable();
            $table->date('release_date')->nullable();
            $table->timestamps();
        });

        Schema::table('gpu_chipsets', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gpu_chipsets');
    }
}
