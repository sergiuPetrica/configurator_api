<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mainboards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('series');
            $table->string('form_factor');
            $table->string('size');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code');
            $table->unsignedBigInteger('chipset_id');
            $table->unsignedBigInteger('socket_id');
            $table->string('raid');
            $table->string('igpu');
            $table->string('audio_chipset');
            $table->string('audio_channels');
            $table->string('network_chipset');
            $table->string('network_speed');
            $table->unsignedBigInteger('memory_type_id');
            $table->string('memory_slot_size');
            $table->unsignedSmallInteger('memory_max_amount_gb');
            $table->unsignedTinyInteger('memory_slots');
            $table->unsignedTinyInteger('memory_channels');
            $table->unsignedSmallInteger('memory_max_speed_mhz');
            $table->boolean('memory_ecc');
            $table->boolean('crossfire');
            $table->boolean('sli');
            $table->json('m2_slots');
            $table->json('io_video');
            $table->json('io_audio');
            $table->json('io_usb');
            $table->json('io_lan');
            $table->json('io_misc');
            $table->json('internal_audio');
            $table->json('internal_usb');
            $table->json('internal_fans');
            $table->json('internal_storage');
            $table->json('internal_power');
            $table->json('internal_misc');
            $table->string('bluetooth');
            $table->string('wifi');
            $table->string('pcb_color');
            $table->string('led_color');
            $table->string('other_colors');
            $table->string('market_segment');
            $table->date('release_date');
            $table->timestamps();
        });

        Schema::table('mainboards', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
            $table->foreign('chipset_id')->references('id')->on('mainboard_chipsets');
            $table->foreign('socket_id')->references('id')->on('sockets');
            $table->foreign('memory_type_id')->references('id')->on('memory_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainboards');
    }
}
