<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainboardStorageInterfaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mainboard_storage_interface', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mainboard_id');
            $table->unsignedBigInteger('storage_interface_id');
            $table->unsignedInteger('number_of_slots');
            $table->timestamps();
        });

        Schema::table('mainboard_storage_interface', function (Blueprint $table) {
            $table->foreign('mainboard_id')->references('id')->on('mainboards');
            $table->foreign('storage_interface_id')->references('id')->on('storage_interfaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainboard_storage_interface');
    }
}
