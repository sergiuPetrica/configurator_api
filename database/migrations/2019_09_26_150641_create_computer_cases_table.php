<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComputerCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computer_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code');
            $table->string('type');
            $table->string('psu_position');
            $table->string('included_psu');
            $table->string('colour');
            $table->unsignedInteger('35_inch_bay_count');
            $table->unsignedInteger('25_inch_bay_count');
            $table->unsignedInteger('expansion_slot_count');
            $table->string('radiator_support');
            $table->string('max_cpu_cooler_height');
            $table->string('max_gpu_length');
            $table->string('dimensions');
            $table->string('led_illumination');
            $table->string('transparent_side_panel');
            $table->json('ports')->default('{}');
            $table->unsignedInteger('included_fan_count');
            $table->unsignedInteger('max_fan_count');
            $table->string('included_fans');
            $table->string('optional_fans');
            $table->timestamps();
        });

        Schema::table('computer_cases', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computer_cases');
    }
}
