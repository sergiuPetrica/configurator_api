<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gpus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('manufacturer_id');
            $table->string('manufacturer_code');
            $table->unsignedBigInteger('expansion_interface_id');
            $table->string('max_resolution')->nullable();
            $table->unsignedBigInteger('chipset_id');
            $table->unsignedSmallInteger('base_clock_mhz');
            $table->unsignedSmallInteger('boost_clock_mhz');
            $table->unsignedSmallInteger('tdp');
            $table->boolean('memory_shared')->default(false);
            $table->unsignedInteger('memory_amount_mb')->nullable();
            $table->string('memory_bus')->nullable();
            $table->unsignedSmallInteger('memory_frequency_mhz')->nullable();
            $table->unsignedInteger('memory_frequency_effective_mhz')->nullable();
            $table->string('memory_bandwidth')->nullable();
            $table->string('pixel_fill_rate')->nullable();
            $table->string('texture_fill_rate')->nullable();
            $table->unsignedTinyInteger('vga_port_count')->nullable();
            $table->unsignedTinyInteger('dvi_port_count')->nullable();
            $table->unsignedTinyInteger('hdmi_port_count')->nullable();
            $table->unsignedTinyInteger('dp_port_count')->nullable();
            $table->unsignedTinyInteger('mini_hdmi_port_count')->nullable();
            $table->unsignedTinyInteger('mini_dp_port_count')->nullable();
            $table->string('shader_model')->nullable();
            $table->string('dx_support')->nullable();
            $table->string('opengl_support')->nullable();
            $table->string('opencl_support')->nullable();
            $table->string('vulkan_support')->nullable();
            $table->boolean('freesync_support')->nullable();
            $table->boolean('gsync_support')->nullable();
            $table->boolean('cooling_system_active')->nullable();
            $table->unsignedTinyInteger('cooling_system_type')->nullable();
            $table->string('cooling_system_slots')->nullable();
            $table->string('length')->nullable();
            $table->string('power_connectors')->nullable();
            $table->date('release_date')->nullable();
            $table->timestamps();
        });

        Schema::table('gpus', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
            $table->foreign('expansion_interface_id')->references('id')->on('expansion_interfaces');
            $table->foreign('chipset_id')->references('id')->on('gpu_chipsets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gpus');
    }
}
