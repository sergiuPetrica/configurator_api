<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase {
    use DatabaseTransactions;

    public function createApplication() {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
