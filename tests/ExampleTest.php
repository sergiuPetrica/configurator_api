<?php

class ExampleTest extends TestCase {
    public function testExample() {
        $this->get('/');

        $this->assertEquals('You found it.', $this->response->getContent());
    }
}
