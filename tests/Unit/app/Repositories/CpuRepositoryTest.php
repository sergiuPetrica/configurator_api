<?php

use App\Repositories\CpuRepository;
use App\Repositories\Resources\CpuCollectionNewegg;
use App\Repositories\Resources\CpuCollectionTechpowerup;

class CpuRepositoryTest extends TestCase {
    public function testGetFromJsonTechpowerup(): void {
        $resource = CpuCollectionTechpowerup::makeEmpty();
        $repository = new CpuRepository($resource);
        $result = $repository->getFromJson(storage_path('components/test/cpus_techpowerup_test.json'));

        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('name', $result[0]);

        $this->assertArrayHasKey('manufacturer', $result[0]);
        $this->assertIsArray($result[0]['manufacturer']);
        $this->assertArrayHasKey('name', $result[0]['manufacturer']);
        $this->assertArrayHasKey('description', $result[0]['manufacturer']);

        $this->assertArrayHasKey('manufacturer_code', $result[0]);

        $this->assertArrayHasKey('socket', $result[0]);
        $this->assertIsArray($result[0]['socket']);
        $this->assertArrayHasKey('name', $result[0]['socket']);

        $this->assertArrayHasKey('codename', $result[0]);
        $this->assertArrayHasKey('generation', $result[0]);
        $this->assertArrayHasKey('fab_process', $result[0]);
        $this->assertArrayHasKey('die_size', $result[0]);
        $this->assertArrayHasKey('core_count', $result[0]);
        $this->assertArrayHasKey('thread_count', $result[0]);
        $this->assertArrayHasKey('smp_count', $result[0]);
        $this->assertArrayHasKey('base_frequency_mhz', $result[0]);
        $this->assertArrayHasKey('turbo_frequency_mhz', $result[0]);
        $this->assertArrayHasKey('l1_cache', $result[0]);
        $this->assertArrayHasKey('l2_cache', $result[0]);
        $this->assertArrayHasKey('l3_cache', $result[0]);
        $this->assertArrayHasKey('tdp', $result[0]);
        $this->assertArrayHasKey('includes_cooler', $result[0]);
        $this->assertArrayHasKey('unlocked', $result[0]);

        $this->assertArrayHasKey('memory_type', $result[0]);
        $this->assertIsArray($result[0]['memory_type']);
        $this->assertArrayHasKey('name', $result[0]['memory_type']);

        $this->assertArrayHasKey('memory_max_amount_gb', $result[0]);
        $this->assertArrayHasKey('memory_frequency_mhz', $result[0]);
        $this->assertArrayHasKey('memory_channels', $result[0]);
        $this->assertArrayHasKey('tcase_max', $result[0]);
        $this->assertArrayHasKey('igpu', $result[0]);
        $this->assertArrayHasKey('market_segment', $result[0]);
        $this->assertArrayHasKey('release_date', $result[0]);
    }

    public function testGetFromJsonNewegg(): void {
        $resource = CpuCollectionNewegg::makeEmpty();
        $repository = new CpuRepository($resource);
        $result = $repository->getFromJson(storage_path('components/test/cpus_newegg_test.json'));

        $this->assertIsArray($result);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('name', $result[0]);

        $this->assertArrayHasKey('manufacturer', $result[0]);
        $this->assertIsArray($result[0]['manufacturer']);
        $this->assertArrayHasKey('name', $result[0]['manufacturer']);
        $this->assertArrayHasKey('description', $result[0]['manufacturer']);

        $this->assertArrayHasKey('manufacturer_code', $result[0]);

        $this->assertArrayHasKey('socket', $result[0]);
        $this->assertIsArray($result[0]['socket']);
        $this->assertArrayHasKey('name', $result[0]['socket']);

        $this->assertArrayHasKey('codename', $result[0]);
        $this->assertArrayHasKey('generation', $result[0]);
        $this->assertArrayHasKey('fab_process', $result[0]);
        $this->assertArrayHasKey('die_size', $result[0]);
        $this->assertArrayHasKey('core_count', $result[0]);
        $this->assertArrayHasKey('thread_count', $result[0]);
        $this->assertArrayHasKey('smp_count', $result[0]);
        $this->assertArrayHasKey('base_frequency_mhz', $result[0]);
        $this->assertArrayHasKey('turbo_frequency_mhz', $result[0]);
        $this->assertArrayHasKey('l1_cache', $result[0]);
        $this->assertArrayHasKey('l2_cache', $result[0]);
        $this->assertArrayHasKey('l3_cache', $result[0]);
        $this->assertArrayHasKey('tdp', $result[0]);
        $this->assertArrayHasKey('includes_cooler', $result[0]);
        $this->assertArrayHasKey('unlocked', $result[0]);

        $this->assertArrayHasKey('memory_type', $result[0]);
        $this->assertIsArray($result[0]['memory_type']);
        $this->assertArrayHasKey('name', $result[0]['memory_type']);

        $this->assertArrayHasKey('memory_max_amount_gb', $result[0]);
        $this->assertArrayHasKey('memory_frequency_mhz', $result[0]);
        $this->assertArrayHasKey('memory_channels', $result[0]);
        $this->assertArrayHasKey('tcase_max', $result[0]);
        $this->assertArrayHasKey('igpu', $result[0]);
        $this->assertArrayHasKey('market_segment', $result[0]);
        $this->assertArrayHasKey('release_date', $result[0]);
    }
}
