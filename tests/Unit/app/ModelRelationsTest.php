<?php

use App\ComputerCase;
use App\Cpu;
use App\CpuCooler;
use App\ExpansionInterface;
use App\Gpu;
use App\GpuChipset;
use App\Hdd;
use App\Mainboard;
use App\MainboardChipset;
use App\Manufacturer;
use App\MemoryType;
use App\Psu;
use App\RamKit;
use App\Socket;
use App\Ssd;
use App\StorageInterface;

class ModelRelationsTest extends TestCase {
    public function testManufacturerRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $mainboard = factory(Mainboard::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $mainboard_chipset = factory(MainboardChipset::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $cpu = factory(Cpu::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $gpu = factory(Gpu::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $gpu_chipset = factory(GpuChipset::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $hdd = factory(Hdd::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $ssd = factory(Ssd::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $psu = factory(Psu::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $computer_case = factory(ComputerCase::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);

        $this->assertTrue($manufacturer->mainboards->first()->id === $mainboard->id);
        $this->assertTrue($manufacturer->mainboard_chipsets->first()->id === $mainboard_chipset->id);
        $this->assertTrue($manufacturer->cpus->first()->id === $cpu->id);
        $this->assertTrue($manufacturer->gpus->first()->id === $gpu->id);
        $this->assertTrue($manufacturer->gpu_chipsets->first()->id === $gpu_chipset->id);
        $this->assertTrue($manufacturer->hdds->first()->id === $hdd->id);
        $this->assertTrue($manufacturer->ssds->first()->id === $ssd->id);
        $this->assertTrue($manufacturer->psus->first()->id === $psu->id);
        $this->assertTrue($manufacturer->computer_cases->first()->id === $computer_case->id);
    }

    public function testMainboardRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $chipset = factory(MainboardChipset::class)->create();
        $socket = factory(Socket::class)->create();
        $memory_type = factory(MemoryType::class)->create();
        $mainboard = factory(Mainboard::class)->create([
            'manufacturer_id' => $manufacturer->id,
            'chipset_id' => $chipset->id,
            'socket_id' => $socket->id,
            'memory_type_id' => $memory_type->id
        ]);
        $expansion_interfaces = factory(ExpansionInterface::class, 5)->create();

        foreach ($expansion_interfaces as $interface) {
            $mainboard->expansion_interfaces()->save($interface, ['number_of_slots' => 2]);
        }

        $storage_interfaces = factory(StorageInterface::class, 5)->create();

        foreach ($storage_interfaces as $interface) {
            $mainboard->storage_interfaces()->save($interface, ['number_of_slots' => 2]);
        }

        $this->assertTrue($mainboard->manufacturer->id === $manufacturer->id);
        $this->assertTrue($mainboard->chipset->id === $chipset->id);
        $this->assertTrue($mainboard->socket->id === $socket->id);
        $this->assertTrue($mainboard->memory_type->id === $memory_type->id);
        $this->assertTrue(count($mainboard->storage_interfaces) === 5);
        $this->assertTrue($mainboard->storage_interfaces->first()->pivot->number_of_slots === 2);
    }

    public function testMainboardChipsetRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $chipset = factory(MainboardChipset::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        factory(Mainboard::class, 5)->create([
            'chipset_id' => $chipset->id
        ]);

        $this->assertTrue($chipset->manufacturer->id === $manufacturer->id);
        $this->assertTrue(count($chipset->mainboards) === 5);
    }

    public function testCpuRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $socket = factory(Socket::class)->create();
        $cpu = factory(Cpu::class)->create([
            'manufacturer_id' => $manufacturer->id,
            'socket_id' => $socket->id
        ]);

        $this->assertTrue($cpu->manufacturer->id === $manufacturer->id);
        $this->assertTrue($cpu->socket->id === $socket->id);
    }

    public function testSocketRelations() {
        $socket = factory(Socket::class)->create();
        factory(Cpu::class, 5)->create([
            'socket_id' => $socket->id
        ]);
        factory(Mainboard::class, 5)->create([
            'socket_id' => $socket->id
        ]);

        $this->assertTrue(count($socket->cpus) === 5);
        $this->assertTrue(count($socket->mainboards) === 5);
    }

    public function testRamKitRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $memory_type = factory(MemoryType::class)->create();
        $ram_kit = factory(RamKit::class)->create([
            'manufacturer_id' => $manufacturer->id,
            'memory_type_id' => $memory_type->id
        ]);

        $this->assertTrue($ram_kit->manufacturer->id === $manufacturer->id);
        $this->assertTrue($ram_kit->memory_type->id === $memory_type->id);
    }

    public function testMemoryTypeRelations() {
        $memory_type = factory(MemoryType::class)->create();
        factory(Mainboard::class, 5)->create([
            'memory_type_id' => $memory_type->id
        ]);
        factory(RamKit::class, 5)->create([
            'memory_type_id' => $memory_type->id
        ]);

        $this->assertTrue($memory_type->mainboards->count() === 5);
        $this->assertTrue($memory_type->ram_kits->count() === 5);
    }

    public function testHddRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $storage_interface = factory(StorageInterface::class)->create();
        $hdd = factory(Hdd::class)->create([
            'manufacturer_id' => $manufacturer->id,
            'storage_interface_id' => $storage_interface->id
        ]);

        $this->assertTrue($hdd->manufacturer->id === $manufacturer->id);
        $this->assertTrue($hdd->storage_interface->id === $storage_interface->id);
    }

    public function testSsdRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $storage_interface = factory(StorageInterface::class)->create();
        $ssd = factory(Ssd::class)->create([
            'manufacturer_id' => $manufacturer->id,
            'storage_interface_id' => $storage_interface->id
        ]);

        $this->assertTrue($ssd->manufacturer->id === $manufacturer->id);
        $this->assertTrue($ssd->storage_interface->id === $storage_interface->id);
    }

    public function testStorageInterfaceRelations() {
        $storage_interface = factory(StorageInterface::class)->create();
        $mainboards = factory(Mainboard::class, 5)->create();

        foreach ($mainboards as $mainboard) {
            $mainboard->storage_interfaces()->save($storage_interface, ['number_of_slots' => 2]);
        }

        $hdds = factory(Hdd::class, 5)->create([
            'storage_interface_id' => $storage_interface->id
        ]);

        $this->assertTrue($storage_interface->mainboards->count() === 5);
        $this->assertTrue($storage_interface->mainboards->first()->id === $mainboards->first()->id);
        $this->assertTrue($storage_interface->mainboards->first()->pivot->number_of_slots === 2);
        $this->assertTrue($storage_interface->hdds->count() === 5);
        $this->assertTrue($storage_interface->hdds->first()->id === $hdds->first()->id);
    }

    public function testGpuRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $chipset = factory(GpuChipset::class)->create();
        $expansion_interface = factory(ExpansionInterface::class)->create();
        $gpu = factory(Gpu::class)->create([
            'manufacturer_id' => $manufacturer->id,
            'chipset_id' => $chipset->id,
            'expansion_interface_id' => $expansion_interface->id
        ]);

        $this->assertTrue($gpu->manufacturer->id === $manufacturer->id);
        $this->assertTrue($gpu->chipset->id === $chipset->id);
        $this->assertTrue($gpu->expansion_interface->id === $expansion_interface->id);
    }

    public function testGpuChipsetRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $chipset = factory(GpuChipset::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        factory(Gpu::class, 5)->create([
            'chipset_id' => $chipset->id
        ]);

        $this->assertTrue($chipset->manufacturer->id === $manufacturer->id);
        $this->assertTrue(count($chipset->gpus) === 5);
    }

    public function testExpansionInterfaceRelations() {
        $expansion_interface = factory(ExpansionInterface::class)->create();
        $mainboards = factory(Mainboard::class, 5)->create();

        foreach ($mainboards as $mainboard) {
            $mainboard->expansion_interfaces()->save($expansion_interface, ['number_of_slots' => 2]);
        }

        $gpus = factory(Gpu::class, 5)->create([
            'expansion_interface_id' => $expansion_interface->id
        ]);

        $this->assertTrue($expansion_interface->mainboards->count() === 5);
        $this->assertTrue($expansion_interface->mainboards->first()->id === $mainboards->first()->id);
        $this->assertTrue($expansion_interface->mainboards->first()->pivot->number_of_slots === 2);
        $this->assertTrue($expansion_interface->gpus->count() === 5);
        $this->assertTrue($expansion_interface->gpus->first()->id === $gpus->first()->id);
    }

    public function testPsuRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $psu = factory(Psu::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);

        $this->assertTrue($psu->manufacturer->id === $manufacturer->id);
    }

    public function testComputerCaseRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $computer_case = factory(ComputerCase::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);

        $this->assertTrue($computer_case->manufacturer->id === $manufacturer->id);
    }

    public function testCpuCoolerRelations() {
        $manufacturer = factory(Manufacturer::class)->create();
        $cpu_cooler = factory(CpuCooler::class)->create([
            'manufacturer_id' => $manufacturer->id
        ]);
        $sockets = factory(Socket::class, 5)->create();

        foreach ($sockets as $socket) {
            $cpu_cooler->sockets()->save($socket);
        }

        $this->assertTrue($cpu_cooler->manufacturer->id === $manufacturer->id);
        $this->assertTrue($cpu_cooler->sockets->count() === 5);
        $this->assertTrue($cpu_cooler->sockets->first()->id === $sockets->first()->id);
    }
}
